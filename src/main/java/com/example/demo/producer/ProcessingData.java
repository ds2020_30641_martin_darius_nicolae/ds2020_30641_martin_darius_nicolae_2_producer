package com.example.demo.producer;

import com.example.demo.dto.Activity;
import com.fasterxml.jackson.databind.util.JSONPObject;
import netscape.javascript.JSObject;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.ClientInfoStatus;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;

public class ProcessingData {

    public ArrayList<Activity> prelucreaza(){
        ArrayList<Activity> lista=new ArrayList<Activity>();
        try {
            File myObj = new File("activity.txt");
            Scanner myReader = new Scanner(myObj);
            UUID id= UUID.randomUUID();
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                //System.out.println(data);
                String start=data.substring(0,19);
                String stop=data.substring(21,40);
                String name=data.substring(42,data.length()-1);

                Activity activity=new Activity(id,name,start,stop);
                lista.add(activity);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return lista;
    }
}
