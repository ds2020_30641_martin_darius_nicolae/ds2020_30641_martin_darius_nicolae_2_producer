package com.example.demo.producer;

import com.example.demo.config.ConfigurationClass;
import com.example.demo.dto.Activity;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/activity")
public class ActivityProducer {
    @Autowired
    private RabbitTemplate template;
    private ProcessingData data=new ProcessingData();

    @PostMapping()
    public String sendActivity() throws JSONException, InterruptedException {
        ArrayList<Activity> lista=data.prelucreaza();
        for(Activity activity:lista){

            JSONObject activityJSON=new JSONObject();
            activityJSON.put("patient_id",activity.getPatient_id());
            activityJSON.put("activity",activity.getActivity());
            activityJSON.put("start",activity.getStart());
            activityJSON.put("end",activity.getEnd());
            System.out.println(activityJSON.toString());


            template.convertAndSend(ConfigurationClass.EXCHANGE, ConfigurationClass.ROUTING_KEY, activity);
            TimeUnit.SECONDS.sleep(1);
        }

        return "Success !!";
    }
}
